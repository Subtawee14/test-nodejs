const express = require("express");
require("./db/mongoose");
const app = express();
const port = process.env.PORT || 3000;
const taskRouter = require("./router/task");
const userRouter = require("./router/user");
app.use(express.json());
app.use(taskRouter);
app.use(userRouter);
app.listen(port, () => {
  console.log("Server is up on port " + port);
});

app.get("/", (req, res) => {
  res.send("OK Baby")
});
